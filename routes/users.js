var express = require('express');
var router = express.Router();
var userService = require('../services/service.user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({error: "Invalid ID."});
});

/* GET users by ID. */
router.get('/:id', async function(req, res, next) {
  const user = await userService.retrieve(req.params.id);
  return res.json({ user: user });
});

/* POST to register a new user */
router.post("/save/registration", async function (req, res) {
  console.log("Saving Registration Information");
  console.log(req.body);

  const user = await userService.create(req.body);
  console.log("From the userService after registration:");
  console.log(user);
  return res.status(201).json({ user: user });
});

/* POST to login a user */
router.post("/login", async function (req, res) {
  console.log("Checking Login Credentials");
  console.log(req.body);

  const user = await userService.verify(req.body);

  return res.status(200).json({ user: user});
});

module.exports = router;
